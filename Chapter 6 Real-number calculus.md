#wip   
# Overview
{{Chapter overview description}}

## Related Mathematics

# 6.1 'Honest' functions

# 6.2 Slopes of functions

# 6.3 Higher derivatives and $C^{\\infty}$-smooth functions

# 6.4 Eulerian notion of functions

# 6.5 Rules of differentiation

# 6.6 Rules of integration