 _Cauchy's Theorem_ states that if $f(z)$ is analytic everywhere  within a simply-connected region then the contour integral for every closed contour lying withing the region equals $0$. 
 
 ## Formula
$$  
\oint\limits_{C}f(z)\,\mathrm{d}z = 0
$$

- $f(z) =$	 function of complex value z
- $C =$	 any closed contour in an analytic region of $f(z)$
