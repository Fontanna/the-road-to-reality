#wip 
# Overview
{{Chapter overview description}}

## Related Mathematics
- [[Taylor series]]
- [[Homotopic deformation]]

# 7.1 Complex smoothness; holomorphic functions

# 7.2 Contour integration

# 7.3 Power series from complex smoothness

# 7.4 Analytic continuation